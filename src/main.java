import java.util.ArrayList;

public class main {
    public static void main(String args[]) {
        Country israel;
        ArrayList<RegularShip> listOfShips = new ArrayList<RegularShip>();
        ArrayList<RegularShip> sortedByTypesList = new ArrayList<RegularShip>();
        ArrayList<RegularShip> sortedByFreigthList = new ArrayList<RegularShip>();

        listOfShips.add(new RegularShip(1, 86.2,"north"));
        listOfShips.add(new CargoShip(2, 35.8,"west",1008.6));
        listOfShips.add(new WarShip(3, 125.9,"east",120,120));
        listOfShips.add(new CargoShip(4, 50.9,"south",754.1));
        listOfShips.add(new RegularShip(5, 100,"west"));
        listOfShips.add(new WarShip(6, 125.9,"east",120,120));
        listOfShips.add(new RegularShip(7, 56.4,"south"));
        listOfShips.add(new CargoShip(8, 87.1,"west",403.5));
        listOfShips.add(new WarShip(9, 156.7,"north",100,160));
        listOfShips.add(new CargoShip(10, 23.9,"west",754.1));

        israel = new Country(listOfShips);

        sortedByTypesList = israel.sortShipsByType();
        System.out.println("sort the ships list by types");
        israel.printShip(sortedByTypesList);

        sortedByFreigthList = israel.sortShipsByCargo();
        System.out.println("\nsort the ships list by cargo");
        israel.printShip(sortedByFreigthList);

        ((WarShip) israel.ships.get(2)).shoot(5,10);
        System.out.println("\nship nu." + israel.ships.get(2).getId() + " shoot");

        System.out.println("\nsort the ships list by types");
        israel.printShip(sortedByTypesList);

        ((CargoShip) israel.ships.get(1)).addCargo(1000);
        System.out.println("\nadd more cargo to ship nu." + israel.ships.get(1).getId());

        System.out.println("\nsort the ships list by cargo");
        israel.printShip(sortedByFreigthList);

        ((CargoShip) israel.ships.get(1)).removeCargo(1000);
        System.out.println("\nremove cargo from ship nu." + israel.ships.get(1).getId());

        System.out.println("\nsort the ships list by cargo");
        israel.printShip(sortedByFreigthList);
    }
}
