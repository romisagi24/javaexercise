public class CargoShip extends RegularShip {

    private double cargo;

    public CargoShip(int id, double speed, String direction, double cargo) {
        super(id, speed, direction);
        this.cargo = cargo;
    }

    public double getCargo() {
        return cargo;
    }

    public void setCargo(double cargo) {
        this.cargo = cargo;
    }

    public void speedUp() {
        int increasingSpeed = 5;

        super.increaseSpeed(increasingSpeed);
    }

    public void speedDown() {
        int decreasingSpeed = 5;

        super.decreaseSpeed(decreasingSpeed);
    }

    public void addCargo(double additionalCargo) {
        this.cargo += additionalCargo;
    }

    public void removeCargo(double excessCargo) {
        this.cargo -= excessCargo;
    }

    @Override
    public String toString() {
        return "CargoShip{" +
                "ID=" + getId() +
                ", speed=" + getSpeed() +
                ", direction='" + getDirection() + '\'' +
                "cargo=" + cargo +
                '}';
    }
}
