import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Country {
    ArrayList<RegularShip> ships = new ArrayList<RegularShip>();

    public Country(ArrayList<RegularShip> listOfShips) {
        this.ships = listOfShips;
    }

    public void addShip(RegularShip regularShip) {
        this.ships.add(regularShip);
    }

    public void deleteShip(int id) {
        this.ships.removeIf(n -> (n.getId() == id));
    }

    public void getShipById(int id) {
        this.ships.stream().filter(
                sample -> sample.getId() == id
        ).forEach(System.out::println);
    }

    public ArrayList<RegularShip> sortShipsByType() {
        ArrayList<RegularShip> newListOfShips = new ArrayList<>();

        for(RegularShip ship: this.ships){
            newListOfShips.add(ship);
        }

        newListOfShips.sort(Comparator.comparing(ship -> ship instanceof WarShip || ship instanceof CargoShip)
                .thenComparing(ship -> ship instanceof WarShip)
                .thenComparing(ship -> ship instanceof CargoShip));

        return newListOfShips;
    }

    public ArrayList<RegularShip> sortShipsByCargo() {
        ArrayList<RegularShip> newListOfShips = new ArrayList<>();

        for(RegularShip ship: this.ships){
            if(ship instanceof CargoShip) {
                newListOfShips.add(ship);
            }
        }

        Collections.sort(newListOfShips, new Comparator<RegularShip>() {
            @Override
            public int compare(RegularShip p1, RegularShip p2)
            {
                double CargoDifference= ((CargoShip) p1).getCargo() - ((CargoShip) p2).getCargo();
                if(CargoDifference > 0) return 1;
                if(CargoDifference < 0) return -1;
                return 0;
            }
        });

        return newListOfShips;
    }

    public void printShip(ArrayList<RegularShip> listOfShips) {
        listOfShips.forEach(System.out::println);
    }
}