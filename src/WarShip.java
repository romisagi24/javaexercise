public class WarShip extends RegularShip {

    private int bombs;
    private int missbles;

    public WarShip(int id, double speed, String direction, int bombs, int missbles) {
        super(id, speed, direction);
        this.bombs = bombs;
        this.missbles = missbles;
    }

    public int getBombs() {
        return bombs;
    }

    public void setBombs(int bombs) {
        this.bombs = bombs;
    }

    public int getMissbles() {
        return missbles;
    }

    public void setMissbles(int missbles) {
        this.missbles = missbles;
    }

    public void shoot(int shootBombs, int shootMissbles) {
        this.setBombs(this.bombs - shootBombs);
        this.setMissbles(this.missbles - shootMissbles);
    }

    @Override
    public String toString() {
        return "WarShip{" +
                "ID=" + super.getId() +
                ", speed=" + super.getSpeed() +
                ", direction='" + super.getDirection() + '\'' +
                "bombs=" + bombs +
                ", missbles=" + missbles +
                '}';
    }
}
