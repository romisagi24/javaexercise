public class RegularShip {

    private int id;
    private double speed;
    private String direction;


    public RegularShip(int id, double speed, String direction) {
        this.id = id;
        this.speed = speed;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public void setID(int ID) {
        this.id = ID;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void increaseSpeed(double accelerationSpeed) {
        try{
            if(accelerationSpeed > 0) {
                this.speed += accelerationSpeed;
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void decreaseSpeed(double decelerationSpeed) {
        this.setSpeed(this.speed += decelerationSpeed);
    }

    public void changingDirection(String newDirection) {
        this.setDirection(newDirection);
    }

    public void speedUp() {
        int increasingSpeed = 20;

        this.increaseSpeed(increasingSpeed);
    }

    public void speedDown() {
        int decreasingSpeed = 20;

        this.decreaseSpeed(decreasingSpeed);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "ID=" + id +
                ", speed=" + speed +
                ", direction='" + direction + '\'' +
                '}';
    }
}
